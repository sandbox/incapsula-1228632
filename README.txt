===Incapsula
URL: http://support.incapsula.com/forums/20158871-extensions
Tags: Incapsula, CDN, comment, ip, performance, speed, security, SQL Injection, spam, x-forwarded-for
Requires at least: 3.0
Tested up to: 6.0

This plugin ensures that your Drupal website runs optimally when using Incapsula

=== Description ===

By using this plugin you will have no change to your originating IPs when using Incapsula. Incapsula acts as a reverse proxy and all incoming connections to your website first pass through one of Incapsula's servers. This plugin will ensure that you continue to see the real originating IP of your website visitors.

The plugin is **highly recommended** for Drupal users using the Incapsula service.

HOW IT WORKS:

* The plugin sets the value of $_SERVER['REMOTE_ADDR'] according to the client IP value reported by the Incapsula proxy.

ABOUT INCAPSULA:

Incapsula gives any website the security and performance previously only available to the website elite. Through a simple DNS settings change, website traffic is seamlessly routed through Incapsula's global network of high-powered servers. Incoming traffic is intelligently profiled, in real-time, blocking even the latest web threats from sophisticated SQL injection attacks to malicious bots and intruding comment spammers. Meanwhile, outgoing traffic is accelerated and optimized for faster load times, keeping welcome visitors speeding through.

Sign up here for Incapsula: [Incapsula Sign up](http://www.incapsula.com/sign-up)


=== Usage ===

This plugin must run first - before all other plugins.
Manualy deactivate and activate the Incapsula plugin after installing any new plugin.

=== Installation ===

1. Unzip the file into the Plugins directory
2. Login as admin
3. Go to the "Plugins" menu and activate "Incapsula".
